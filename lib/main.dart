import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:formvalidation/src/blocs/provider.dart';
import 'package:formvalidation/src/pages/home_page.dart';
import 'package:formvalidation/src/pages/login_page.dart';
import 'package:formvalidation/src/pages/producto_page.dart';
import 'package:formvalidation/src/pages/registro_page.dart';
import 'package:formvalidation/src/preferences/preferencias_usuario.dart';
 
void main() async {
  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();
  runApp(MyApp());
}
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final prefs =  new PreferenciasUsuario();
    print(prefs);
    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        initialRoute: (prefs.token != '') ? 'home' : 'login',
        routes: {
          'login'     :     (BuildContext context) => LoginPage(),
          'registro'  :     (BuildContext context) => RegistroPage(),
          'home'      :     (BuildContext context) => HomePage(),
          'producto'  :     (BuildContext context) => ProductoPage(),
        },
        theme: ThemeData(
          primaryColor: Colors.deepPurple
        ),
      ),
    );
  }
}